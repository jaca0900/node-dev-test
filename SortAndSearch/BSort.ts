export class BSort {
  constructor() {}

  private swap(dataSet: number[], i: number, j: number) {
    const temp = dataSet[i];
    dataSet[i] = dataSet[j];
    dataSet[j] = temp;
  }

  public quickSort(dataSet: number[], begin: number, end: number) {
    if(end <= begin) {
      return dataSet;
    }

    let i = begin - 1;
    let j = end + 1;

    let pivot = dataSet[Math.floor((begin + end)/2)];

    while(true) {
      while(pivot > dataSet[++i]);

      while(pivot < dataSet[--j]);

      if (i > j) {
        break;
      }

      this.swap(dataSet, i, j);
    }

    if(j > begin) {
      this.quickSort(dataSet, begin, j);
    }

    if(i < end) {
      this.quickSort(dataSet, i, end);
    }

    return dataSet;
  }
}