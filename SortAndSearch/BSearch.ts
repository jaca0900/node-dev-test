export class BSearch {
  private static instance: BSearch;
  private _operations: number;

  private constructor() {}

  static getInstance() {
    if (! this.instance) {
      this.instance = new BSearch();
    }

    return this.instance;
  }

  public binarySearch(dataSet: number[], element: number, order: boolean) {
    this._operations = 0;
    let begin = 0;
    this._operations++;
    let end = dataSet.length - 1;
    this._operations++;
    let previousPivot = dataSet[0];
    this._operations++;
    let middle = Math.floor((begin + end / 2));
    this._operations++;

    while ((previousPivot || previousPivot === 0) &&
      previousPivot !== dataSet[middle]) {

      if (dataSet[middle] === element) {
        this._operations++;
        return middle;
      }

      previousPivot = dataSet[middle];

      if (dataSet[middle] > element) {
        this._operations++;
        if (order) {
          end = middle;
        } else {
          begin = middle;
        }
      } else {
        this._operations++;
        if (order) {
          begin = middle;
        } else {
          end = middle;
        }
      }

      middle = Math.floor((begin + end / 2));
      this._operations++;
    }

    return -1;
  }

  get operations() {
    return this._operations;
  }
}