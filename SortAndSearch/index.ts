import { ASort } from './ASort';
import { BSort } from './BSort';
import { BSearch } from './BSearch';

const unsorted = [13, 2, 17, 5, 77, 22, 83, 65, 14, 9, 0, 4, 7, 32];
const elementsToFind = [1, 5, 13, 27, 77];
const aSort = new ASort();
const bSort = new BSort();

const mergeSorted = aSort.mergeSort([ ...unsorted ]);
const quickSorted = bSort.quickSort([ ...unsorted ], 0, unsorted.length - 1);

const results = elementsToFind.map((element) => {
  return {
    quickSortSearch: {
      element,
      index: BSearch.getInstance().binarySearch(quickSorted, element, true),
      operations: BSearch.getInstance().operations
    },
    mergeSortSearch: {
      element,
      index: BSearch.getInstance().binarySearch(mergeSorted, element, true),
      operations: BSearch.getInstance().operations
    }
  }
});

console.log('mergeSorted', mergeSorted, 'quickSorted', quickSorted, 'Search results', results);