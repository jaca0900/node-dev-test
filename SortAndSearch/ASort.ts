export class ASort {
  constructor() {}

  private merge(leftSet: number[], rightSet: number[]): any[] {

    for (let value of rightSet) {
      const index = leftSet.findIndex((lVal) => {
        return lVal >= value;
      });

      if (index < 0) {
        leftSet = [ ...leftSet, value ];
        continue;
      }

      const temp = leftSet.splice(index);

      leftSet = [ ...leftSet, value, ...temp];
    }

    return leftSet;
  }

  public mergeSort(dataSet: number[]): any[] {
    if (dataSet.length === 1) {
      return dataSet;
    } 

    const pivot: number = Math.floor(dataSet.length / 2);

    let leftSet = dataSet.splice(0, pivot);
    let rightSet = dataSet;

    const leftSorted = this.mergeSort(leftSet); 
    const rightSorted = this.mergeSort(rightSet);

    return this.merge(leftSorted, rightSorted);
  }
}