import { IOrder } from './IOrder';
import { IProduct } from './IProduct';
import { ICustomer } from './ICustomer';
import { IBestBuyers, IBestSellers } from 'src/Report/Model/IReports';

export class Order {
  private number: string;
  private createdAt: string;
  private customer: ICustomer;
  private products: IProduct[];

  constructor(order: IOrder, productList: IProduct[] = [], customerList: ICustomer[] = []) {
    this.number = order.number;

    this.customer = customerList.find((customer) => customer.id === order.customer);

    this.products = order.products.map((product) => {
      return productList.find((prod) => prod.id === product);
    });
  }

  public get totalOrderPrice(): number {
    return this.products.reduce((acc, product) => acc += product.price, 0);
  }

  public getTime(): number {
    return new Date(this.number).getTime();
  }

  get buyer(): IBestBuyers {
    return {
      customerName: `${this.customer.firstName} ${this.customer.lastName}`,
      totalPrice: this.totalOrderPrice
    }
  }

  get bestSellers(): IBestSellers[] {
    return this.products.map((product) => ({
      productName: product.name,
      quantity: 1,
      totalPrice: product.price,
    })).reduce((acc, product) => {
      const productIndex = acc.findIndex((buyer) => buyer.productName === product.productName);

      if (productIndex < 0) {
        acc.push(product);
      } else {
        acc[productIndex].totalPrice += product.totalPrice;
        acc[productIndex].quantity += product.quantity;
      }

      return acc;
    }, []);
  }
}