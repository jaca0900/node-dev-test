export interface IOrder {
    number: string,
    customer: number,
    createdAt: string,
    products: number[],
}