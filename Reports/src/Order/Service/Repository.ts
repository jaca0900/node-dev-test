import { Injectable } from '@nestjs/common';
import { IOrder } from '../Model/IOrder';
import { ICustomer } from '../Model/ICustomer';
import { IProduct } from '../Model/IProduct';
/**
 * Data layer - mocked
 */
@Injectable()
export class Repository {
  fetchOrders(): Promise<IOrder[]> {
    return new Promise(resolve => resolve(require('../Resources/Data/orders')));
  }

  fetchProducts(): Promise<IProduct[]> {
    return new Promise(resolve =>
      resolve(require('../Resources/Data/products')),
    );
  }

  fetchCustomers(): Promise<ICustomer[]> {
    return new Promise(resolve =>
      resolve(require('../Resources/Data/customers')),
    );
  }
}
