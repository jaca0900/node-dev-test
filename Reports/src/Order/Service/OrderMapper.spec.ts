import * as request from 'supertest';
import { Repository } from './Repository';
import { Test } from '@nestjs/testing';
import { INestApplication } from '@nestjs/common';
import { OrderMapper } from './OrderMapper';

describe('OrderMapper', () => {
  let orderMapper;
  let orderRepository;

  beforeAll(async () => {
    const orderModule = await Test.createTestingModule({
      providers: [ Repository, OrderMapper ]
    }).compile();
    
    orderMapper = orderModule.get<OrderMapper>(OrderMapper);
    orderRepository = orderModule.get<Repository>(Repository);
  });

  it('Should have mapOrders property', () => {
    expect(orderMapper).toHaveProperty('mapOrders');
  })

  it('Should have mapOrdersByDate property', () => {
    expect(orderMapper).toHaveProperty('mapOrdersByDate');
  })

  it('Should have mapBestSellers property', () => {
    
    expect(orderMapper).toHaveProperty('mapBestSellers');
  })

  it('Should have mapBestBuyers property', () => {
    expect(orderMapper).toHaveProperty('mapBestBuyers');
  })

  it('Should return populated bestBuyers with valid date', async () => {
    expect(await orderMapper.mapBestBuyers(new Date('2019-07-1'))).toEqual([
      { customerName: 'John Doe', totalPrice: 381.5 },
      { customerName: 'Jane Doe', totalPrice: 135.75 }
    ]);
  })

  it('Should return populated bestSellers with valid date', async () => {
    expect(await orderMapper.mapBestSellers(new Date('2019-07-1'))).toEqual([
      { productName: 'Black sport shoes', quantity: 4, totalPrice: 440 },
      { productName: 'Cotton t-shirt XL', quantity: 3, totalPrice: 77.25 }
    ]);
  })

  it('Should return empty bestBuyers with invalid date', async () => {
    expect(await orderMapper.mapBestBuyers(new Date('invalid'))).toEqual([]);
  })

  it('Should return empty mapBestBuyers with valid date out of range', async () => {
    expect(await orderMapper.mapBestBuyers(new Date('2456-07-1'))).toEqual([]);
  })

  it('Should return empty bestSellers with valid date out of range', async () => {
    expect(await orderMapper.mapBestSellers(new Date('2456-07-1'))).toEqual([]);
  })

  it('Should return empty mapBestSellers with invalid date', async () => {
    expect(await orderMapper.mapBestSellers(new Date('invalid'))).toEqual([]);
  })
});
