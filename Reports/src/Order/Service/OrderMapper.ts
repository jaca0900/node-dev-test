import { Injectable, Inject } from '@nestjs/common';
import { Repository } from './Repository';
import { Order } from '../Model/Order';
import { IBestBuyers, IBestSellers } from 'src/Report/Model/IReports';

@Injectable()
export class OrderMapper {
  @Inject() repository: Repository;

  /**
   * TESTS:
   * shouldExist
   * mockData and check returned instance and structure
   */
  private async mapOrders (): Promise<Order[]> {
    const orders = await this.repository.fetchOrders();
    const products = await this.repository.fetchProducts();
    const customers = await this.repository.fetchCustomers();

    return orders.map((order) => new Order(order, products, customers));
  }

  private async mapOrdersByDate(date: Date): Promise<Order[]> {
    const mapped = await this.mapOrders();

    return mapped.filter((order) => order.getTime() === date.getTime());
  }

  public async mapBestSellers(date: Date): Promise<IBestSellers[]> {
    const mappedOrders = await this.mapOrdersByDate(date);

    // Flatten and reduce bestSellers array
    return mappedOrders.reduce((acc, order) => {
      const bestSellers = order.bestSellers;

      for (let bestSeller of bestSellers) {
        const sellersIndex = acc.findIndex((seller) => seller.productName === bestSeller.productName);

        if (sellersIndex < 0) {
          acc.push(bestSeller);
        } else {
          acc[sellersIndex].totalPrice += bestSeller.totalPrice;
          acc[sellersIndex].quantity += bestSeller.quantity;
        }
      }

      return acc;
    }, [])
    .sort((sellA, sellB) => sellB.totalPrice - sellA.totalPrice);
  }

  public async mapBestBuyers(date: Date): Promise<IBestBuyers[]> {
    const mappedOrders = await this.mapOrdersByDate(date);

    // reduce the bestBuyers
    return mappedOrders.reduce((acc, order) => {
      const orderBuyer = order.buyer;
      const buyerIndex = acc.findIndex((buyer) => buyer.customerName === orderBuyer.customerName);

      if (buyerIndex < 0) {
        acc.push(orderBuyer);
      } else {
        acc[buyerIndex].totalPrice += orderBuyer.totalPrice;
      }

      return acc;
    }, [])
    .sort((buyA, buyB) => buyB.totalPrice - buyA.totalPrice);
  }
}
