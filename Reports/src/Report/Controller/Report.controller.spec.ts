import * as request from 'supertest';
import { Test } from '@nestjs/testing';
import { ReportModule } from '../ReportModule';
import { ReportController } from '../Controller/ReportController'

describe('ReportController', () => {
  let reportController;
  beforeAll(async () => {
    const reportModule = await Test.createTestingModule({
      imports: [ReportModule],
    }).compile();

    reportController = reportModule.get<ReportController>(ReportController);
  });

  it('should have bestSellersRoute', () => {
    expect(reportController).toHaveProperty('bestSellers');
  });

  it('should have bestBuyersRoute', () => {
    expect(reportController).toHaveProperty('bestBuyers');
  });
});
