import { Controller, Get, Param, Res, Inject } from '@nestjs/common';
import { IBestSellers, IBestBuyers } from '../Model/IReports';
import { OrderMapper } from '../../Order/Service/OrderMapper';

@Controller('report')
export class ReportController {
  @Inject() orderMapper: OrderMapper;

  @Get("/products/:date")
  bestSellers(@Param("date") date: string): Promise<IBestSellers[]> {
    const orderDate = new Date(date);

    return this.orderMapper.mapBestSellers(orderDate);
  }

  @Get("/customer/:date")
  bestBuyers(@Param("date") date: string): Promise<IBestBuyers[]> {
    const orderDate = new Date(date);

    return this.orderMapper.mapBestBuyers(orderDate);
  }
}
