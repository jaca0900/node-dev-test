import * as request from 'supertest';
import { Test } from '@nestjs/testing';
import { INestApplication } from '@nestjs/common';
import { ReportModule } from './../src/Report/ReportModule';
import { Repository } from './../src/Order/Service/Repository';

describe('Reports', () => {
  let app: INestApplication;

  beforeAll(async () => {
    const moduleFixture = await Test.createTestingModule({
      imports: [ReportModule],
    }).compile();

    app = moduleFixture.createNestApplication();
    await app.init();
  });

  it('/ (GET)', () => {
    return request(app.getHttpServer())
      .get('/')
      .expect(404);
  });

  it('/report/products (GET) Empty response', () => {
    return request(app.getHttpServer())
      .get('/report/products/222')
      .expect(200)
      .expect([]);
  });

  it('/report/products (GET) Empty response', () => {
    return request(app.getHttpServer())
      .get('/report/products/2019-07-1')
      .expect(200)
      .expect([
        { productName: 'Black sport shoes', quantity: 4, totalPrice: 440 },
        { productName: 'Cotton t-shirt XL', quantity: 3, totalPrice: 77.25 }
      ]);
  });

  it('/report/customer (GET)', () => {
    return request(app.getHttpServer())
      .get('/report/customer/2019-07-1')
      .expect(200)
      .expect([
        { customerName: 'John Doe', totalPrice: 381.5 },
        { customerName: 'Jane Doe', totalPrice: 135.75 }
      ]);
  });
});
